//
//  Enums.swift
//  ContactBook
//
//  Created by Michal Grman on 21/06/2017.
//  Copyright © 2017 Grman Trial. All rights reserved.
//

import Alamofire

public enum API: CustomStringConvertible {
    
   
    public static let statusCodes = 200..<400
    public static let contactTypes = ["application/json"]
    
    private static let root = "http://private-36f1e-contactstest.apiary-mock.com"
    
    case add
    case contacts
    case orders(id:String)
    
    public var description: String {
        switch self {
        case .contacts,.add:
            return API.root + "/contacts"
        case .orders(let id):
            return API.root + "/contacts/\(id)/order"
        }
    }
    
    public func resolve() -> (url:String, method:HTTPMethod) {
    
        switch self {
        case .contacts: return (url:self.description, method: .get)
        case .add:      return (url:self.description, method: .post)
        case .orders(_):return (url:self.description, method: .get)
        }
    }
}

public enum ServiceError: Error, CustomStringConvertible {
    
    case noData
    case disabled
    case underlying(error:Error)
    
    public var description: String {
        switch self {
        case .noData:
            return "There is not data!"
        case .disabled:
            return "Your action is currently disabled!"
        case .underlying(let error):
            return "There was an error \(error)"
        }
    }
}
