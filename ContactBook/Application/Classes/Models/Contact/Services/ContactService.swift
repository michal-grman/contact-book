//
//  ContactService.swift
//  ContactBook
//
//  Created by Michal Grman on 21/06/2017.
//  Copyright © 2017 Grman Trial. All rights reserved.
//

import ReactiveSwift
import Sync
import Alamofire
import AlamofireObjectMapper
import ObjectMapper

extension Contact {
    
    public class Service: ReactiveExtensionsProvider {
        
        fileprivate let stack:DataStack
        
        public init(stack:DataStack)  {
            self.stack = stack
        }
        
    
        fileprivate func dataRequest(using api:API, parameters:Parameters? = nil) -> DataRequest {
            
            let (url, method) = api.resolve()
            return Alamofire.request(url, method: method, parameters:parameters)
        }
    }
}

extension Reactive where Base: Contact.Service {
    
    public var syncContactsAction:Action<Void,Bool,ServiceError> {
        return Action<Void,Bool,ServiceError> { contact in
            
            return SignalProducer<Bool, ServiceError> { observer, disposable in
                
                let queue = DispatchQueue.global(qos: .utility)
                let request = self.base.dataRequest(using: .contacts)
                
                disposable.add {
                    request.cancel()
                }
                
                request
                    .validate(statusCode: API.statusCodes)
                    .validate(contentType: API.contactTypes)
                    .responseJSON(queue: queue) { response in
                        
                        switch response.result {
                        case .success(let params):
                            if let data = params as? Parameters, let changes = data[Contact.RemoteKeypath] as? [Parameters] {
                                
                                self.base.stack.sync(changes, inEntityNamed: "\(Contact.self)") { (error) in
                                    if let error = error {
                                        observer.send(error: .underlying(error:error))
                                    } else {
                                        observer.send(value: true)
                                        observer.sendCompleted()
                                    }
                                }
                                
                            } else {
                                observer.send(error: .noData)
                            }
                        case .failure(let error):
                            observer.send(error: .underlying(error:error))
                        }
                    }
            }
        }
    }
    
    public var addContactAction:Action<Contact.Form,Bool,ServiceError> {
        return Action<Contact.Form,Bool,ServiceError> { form in
            
            return SignalProducer<Bool, ServiceError> { observer, disposable in
                
                let queue = DispatchQueue.global(qos: .utility)
                let request = self.base.dataRequest(using: .add, parameters: form.toJSON())
                
                disposable.add {
                    request.cancel()
                }
                
                request
                    .validate(statusCode: API.statusCodes)
                    .validate(contentType: API.contactTypes)
                    .response(queue: queue) { response in
                        
                        if let error = response.error {
                            observer.send(error: .underlying(error:error))
                        } else {
                            observer.send(value: true)
                            observer.sendCompleted()
                        }
                }
            }
        }
    }
    
    public var getOrdersAction:Action<Contact?,[Contact.Order],ServiceError> {
        return Action<Contact?,[Contact.Order],ServiceError> { contact in
            
            guard let contact = contact, let id = contact.id else {
                return SignalProducer<[Contact.Order], ServiceError>(error:.noData)
            }
            
            return SignalProducer<[Contact.Order], ServiceError> { observer, disposable in
                
                let queue = DispatchQueue.global(qos: .utility)
                let request = self.base.dataRequest(using: .orders(id: id))
                
                disposable.add {
                    request.cancel()
                }
                
                request
                    .validate(statusCode: API.statusCodes)
                    .validate(contentType: API.contactTypes)
                    .responseArray(queue:queue, keyPath: Contact.Order.RemoteKeypath) { (response: DataResponse<[Contact.Order]>) in
                    
                        switch response.result {
                        case .success:
                            
                            if let orders = response.result.value {
                                observer.send(value: orders)
                                observer.sendCompleted()
                            } else{
                                observer.send(error: .noData)
                            }
                            
                        case .failure(let error):
                            observer.send(error: .underlying(error:error))
                        }
                }
            }
        }
    }
}
