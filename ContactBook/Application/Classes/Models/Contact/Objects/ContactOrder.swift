//
//  ContactOrder.swift
//  ContactBook
//
//  Created by Michal Grman on 21/06/2017.
//  Copyright © 2017 Grman Trial. All rights reserved.
//

import ObjectMapper

extension Contact {
    
    public struct Order: Mappable {
        
        public static let RemoteKeypath = "items"
        
        public var name:String?
        public var count:Int = 0
    
        
        public init?(map: Map) {
        }
        
        mutating public func mapping(map: Map) {
            name 	<- map["name"]
            count 	<- map["count"]
        }
    }
}
