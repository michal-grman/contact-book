//
//  ContactForm.swift
//  ContactBook
//
//  Created by Michal Grman on 21/06/2017.
//  Copyright © 2017 Grman Trial. All rights reserved.
//

import ObjectMapper

extension Contact {
    
    public struct Form: Mappable {
        
        private static let CharCount = 5
        
        public static let Invalid = Form(name: "", phone: "")
        
        public var name:String?
        public var phone:String?
        
        public var valid:Bool {
            
            guard let name = self.name, let phone = self.phone else { return false }
            return name.characters.count > Form.CharCount && phone.characters.count > Form.CharCount
        }
        
        public init(name:String, phone:String) {
            self.name = name; self.phone = phone
        }
        
        public init?(map: Map) {
        }
        
        mutating public func mapping(map: Map) {
            name 	<- map["name"]
            phone 	<- map["phone"]
        }
    }
}

