//
//  ContactDetailViewController.swift
//  ContactBook
//
//  Created by Michal Grman on 21/06/2017.
//  Copyright © 2017 Grman Trial. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa
import PKHUD

extension Contact {
    
    struct DetailViewBindings {
        
        let title:BindingTarget<String>
    
        var phone:BindingTarget<String?> {
            guard let phone = self.controller.phone else { fatalError("Missing phone outlet!") }
            return phone.reactive.text
        }
    
        var picture:UIImageView {
            guard let picture = self.controller.picture else { fatalError("Missing picture outlet!") }
            return picture
        }
        
        unowned let controller:ContactDetailViewController
        
        init(using controller:ContactDetailViewController) {
            self.controller = controller
            self.title = controller.navigationItem.reactive.makeBindingTarget{ (item:UINavigationItem, title:String) in
                item.title = title
            }
        }
    }
}

class ContactDetailViewController: ContactBaseViewController {

    static let Identifier = "\(ContactDetailViewController.self)"
    
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var phone: UILabel?
    @IBOutlet weak var picture: UIImageView?
    @IBOutlet weak var headerView: UIView?
    
    var bindings:Contact.DetailViewBindings?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bindings = Contact.DetailViewBindings(using:self)
        if let vm = self.viewModel as? Contact.DetailViewModel, let bindings = self.bindings {
            
            if let table = self.tableView {
                
                table.register(UINib(nibName: "\(OrderTableViewCell.self)", bundle: nil), forCellReuseIdentifier: OrderTableViewCell.Identifier)
                table.dataSource = self
                table.tableFooterView = UIView()
                table.allowsSelection = false
                table.rowHeight = 45.0;
                
                
                vm.setupBindings(bindings)
                vm.changes
                    .take(duringLifetimeOf: self)
                    .observe(on: UIScheduler())
                    .observeValues { [unowned table] _ in
                        table.reloadData()
                }
            }
        }
        
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.refresh()
    }
    
    func refresh() {
        
        guard let vm = self.viewModel as? Contact.DetailViewModel else { fatalError("Did you forget to inject VM?") }
        vm.getOrdersAction.apply().take(duringLifetimeOf: self).start()
    }
    
    override func handle(withPending pending: Bool) {
        
        if pending { HUD.show(.progress) }
    }
    
    override func handle(withSuccess success: Bool) {
        
        HUD.hide()
    }
    
    override func handle(withError error: ServiceError) {
        
        HUD.hide()
        super.handle(withError: error)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sizeHeaderToFit()
    }
    
    func sizeHeaderToFit() {
        
        if let table = self.tableView, let header = self.headerView {
         
            header.setNeedsLayout()
            header.layoutIfNeeded()
            
            var frame = header.frame
            frame.size.height = 80.0
            header.frame = frame
            
            table.tableHeaderView = header
        }
        
        
    }
}

extension ContactDetailViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let vm = self.viewModel as? Contact.DetailViewModel else { return 0 }
        return vm.orders.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: OrderTableViewCell.Identifier, for: indexPath)
        
        if let binding = cell as? OrderBindingProtocol, let vm = self.viewModel as? Contact.DetailViewModel {
            binding.bind(using: vm.orders.value[indexPath.item])
        }
        
        return cell
    }
}


