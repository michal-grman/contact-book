//
//  ContactAddViewController.swift
//  ContactBook
//
//  Created by Michal Grman on 21/06/2017.
//  Copyright © 2017 Grman Trial. All rights reserved.
//

import UIKit
import Result
import ReactiveSwift
import ReactiveCocoa
import PKHUD

extension Contact {
    
    struct AddViewBindings {
        
        var button:UIButton {
            guard let button = self.controller.addButton else { fatalError("Missing add button outlet!") }
            return button
        }
        
        var name:Signal<String?, NoError> {
            guard let name = self.controller.nameField else { fatalError("Missing name outlet!") }
            return name.reactive.continuousTextValues.take(duringLifetimeOf: self.controller)
        }
        
        var phone:Signal<String?, NoError> {
            guard let phone = self.controller.phoneField else { fatalError("Missing phone outlet!") }
            return phone.reactive.continuousTextValues.take(duringLifetimeOf: self.controller)
        }
        
        unowned let controller:ContactAddViewController
        
        init(using controller:ContactAddViewController) {
            self.controller = controller
        }
    }
}

class ContactAddViewController: ContactBaseViewController {

    static let Identifier = "\(ContactAddViewController.self)"
    
    @IBOutlet weak var nameField: UITextField?
    @IBOutlet weak var phoneField: UITextField?
    @IBOutlet weak var addButton: UIButton?

    var bindings:Contact.AddViewBindings?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.reset()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.bindings = Contact.AddViewBindings(using:self)
        if let vm = self.viewModel as? Contact.AddViewModel, let bindings = self.bindings {
            
            vm.setupBindings(bindings)
        }
        
        self.navigationItem.title = "Add New Contact"
    }
    
    func reset() {
        
        if let name = self.nameField { name.text = "" }
        if let phone = self.phoneField { phone.text = "" }
    }
    
    override func handle(withPending pending: Bool) {
        
        if pending { HUD.show(.progress) }
    }
    
    override func handle(withSuccess success: Bool) {
        
        HUD.flash(.success, delay: 1.0) { [unowned self] finished in
            
            if let nav = self.navigationController {
                nav.popViewController(animated: true)
            }
        }
    }
    
    override func handle(withError error: ServiceError) {
        HUD.hide()
        super.handle(withError: error)
    }
}
