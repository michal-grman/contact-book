//
//  ContactTableViewCell.swift
//  ContactBook
//
//  Created by Michal Grman on 21/06/2017.
//  Copyright © 2017 Grman Trial. All rights reserved.
//

import UIKit
import AlamofireImage

class ContactTableViewCell: UITableViewCell, ContactBindingProtocol {

    deinit {
        print("Deallocated Cell \(type(of: self))")
    }
    
    static let Identifier = "\(ContactTableViewCell.self)"
    
    @IBOutlet weak var name: UILabel?
    @IBOutlet weak var phone: UILabel?
    @IBOutlet weak var picture: UIImageView?
    
    func bind(using contact:Contact) {
        
        if let name = self.name {
            name.text = contact.name
        }
        
        if let phone = self.phone {
            phone.text = contact.phone
        }
        
        if let picture = self.picture {
            
            let placeholder = UIImage(named: "placeholder.png")
            if let path = contact.pictureUrl, let url = URL(string: path) {
                picture.af_setImage(withURL: url, placeholderImage:placeholder, imageTransition:.crossDissolve(0.3))
            } else {
                picture.image = placeholder
            }
        }
    }
}
