//
//  OrderTableViewCell.swift
//  ContactBook
//
//  Created by Michal Grman on 21/06/2017.
//  Copyright © 2017 Grman Trial. All rights reserved.
//

import UIKit

class OrderTableViewCell: UITableViewCell, OrderBindingProtocol {

    deinit {
        print("Deallocated Cell \(type(of: self))")
    }
    
    static let Identifier = "\(OrderTableViewCell.self)"
    
    @IBOutlet weak var name: UILabel?
    @IBOutlet weak var count: UILabel?
    
    func bind(using order:Contact.Order) {
        
        if let name = self.name {
            name.text = order.name
        }
        
        if let count = self.count {
            count.text = "\(order.count)x"
        }
    }
}
