//
//  ContactListViewController.swift
//  ContactBook
//
//  Created by Michal Grman on 21/06/2017.
//  Copyright © 2017 Grman Trial. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa
import DATASource
import ESPullToRefresh

extension Contact {
    
    struct ListViewBindings {
        
        var item:UIBarButtonItem {
            guard let item = self.controller.navigationItem.rightBarButtonItem else { fatalError("Missing add button!") }
            return item
        }
        
        unowned let controller:ContactListViewController
        
        init(using controller:ContactListViewController) {
            self.controller = controller
        }
    }
}

class ContactListViewController: ContactBaseViewController {

    @IBOutlet weak var tableView: UITableView?
    
    var bindings:Contact.ListViewBindings?
    var source:DATASource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Orders"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: nil, action: nil)
        
        self.bindings = Contact.ListViewBindings(using:self)
        if let vm = self.viewModel as? Contact.ListViewModel, let bindings = self.bindings {
        
            if let table = self.tableView {
                
                table.register(UINib(nibName: "\(ContactTableViewCell.self)", bundle: nil), forCellReuseIdentifier: ContactTableViewCell.Identifier)
                table.delegate = self
                table.tableFooterView = UIView()
                table.rowHeight = 70.0;
                table.es.addPullToRefresh { [weak self] in
                    
                    self?.refresh()
                }
                
                do {
                    self.source = try vm.setupSourceBindings(using: table, identifier: ContactTableViewCell.Identifier)
                    
                    table.dataSource = self.source
                    table.reloadData()
                    
                } catch {
                    fatalError("Forget to inject data source stack?")
                }
                
                vm.setupBindings(bindings)
                vm.add
                    .take(duringLifetimeOf: self)
                    .observe(on: UIScheduler())
                    .observeValues { [unowned self] pending in
                        self.performSegue(withIdentifier: ContactAddViewController.Identifier, sender: self)
                }
            }
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.refresh()
    }
    
    
    func refresh() {
        
        guard let vm = self.viewModel as? Contact.ListViewModel else { fatalError("Did you forget to inject VM?") }
        vm.syncContactsAction.apply().take(duringLifetimeOf: self).start()
    }
    
    override func handle(withPending pending: Bool) {
        
        if let table = self.tableView, pending {
            table.es.startPullToRefresh()
        }
    }
    
    override func handle(withSuccess success: Bool) {
        
        if let table = self.tableView {
            
            table.es.stopPullToRefresh()
            table.reloadData()
        }
    }
    
    override func handle(withError error: ServiceError) {
        if let table = self.tableView {
            
            table.es.stopPullToRefresh()
            table.reloadData()
        }
        
        super.handle(withError: error)
    }
}

extension ContactListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if  let source = self.source, let vm = self.viewModel as? Contact.ListViewModel, vm.bindContact(using:source, at:indexPath) {
            
            self.performSegue(withIdentifier: ContactDetailViewController.Identifier, sender: self)
        }
    }
}
