//
//  ContactBaseViewController.swift
//  ContactBook
//
//  Created by Michal Grman on 21/06/2017.
//  Copyright © 2017 Grman Trial. All rights reserved.
//

import UIKit
import ReactiveSwift
import ESPullToRefresh

class ContactBaseViewController: UIViewController {

    deinit {
        print("Deallocated VC \(type(of: self))")
    }
    
    var viewModel:ContactResultViewProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
    
        if let vm = self.viewModel {
        
            vm.pending
                .take(duringLifetimeOf: self)
                .observe(on: UIScheduler())
                .observeValues { [unowned self] pending in
                    self.handle(withPending: pending)
            }
            
            vm.success
                .take(duringLifetimeOf: self)
                .observe(on: UIScheduler())
                .observeValues { [unowned self] success in
                    self.handle(withSuccess: success)
            }
            
            vm.errors
                .take(duringLifetimeOf: self)
                .observe(on: UIScheduler())
                .observeValues { [unowned self] error in
                    self.handle(withError: error)
            }
        }
        
        
    }
    
    func handle(withPending pending:Bool) {
    }
    
    func handle(withSuccess success:Bool) {
    }
    
    func handle(withError error:ServiceError) {
     
        let alert = UIAlertController(title: "Error", message: error.description, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default)
        
        alert.addAction(action)
        self.present(alert, animated: true)
    }
}
