//
//  ContactBookAssembly.swift
//  ContactBook
//
//  Created by Michal Grman on 21/06/2017.
//  Copyright © 2017 Grman Trial. All rights reserved.
//

import SwinjectAutoregistration
import Swinject
import SwinjectStoryboard
import Sync

public func ContactBookAssembly() -> Container {
    return Container { container in
        
        // Models
        container
            .register(DataStack.self) { _ in DataStack(modelName: "ContactModel", storeType: .sqLite) }
            .inObjectScope(.weak)
        
        container
            .autoregister(Contact.Service.self, initializer: Contact.Service.init(stack:))
            .inObjectScope(.container)
        
        // View models
        container
            .autoregister(Contact.AddViewModel.self, initializer: Contact.AddViewModel.init(service:))
            .inObjectScope(.transient)
        
        container
            .autoregister(Contact.DetailViewModel.self, initializer: Contact.DetailViewModel.init(service:))
            .inObjectScope(.container)
        
        container.register(Contact.ListViewModel.self) { r in
            
            let vm = Contact.ListViewModel(service: r ~> Contact.Service.self)
            
            vm.stack = r ~> DataStack.self
            vm.detailViewModel = r ~> Contact.DetailViewModel.self
            
            return vm
        }
        
        // Views
        container.storyboardInitCompleted(UINavigationController.self) { r, c in
        }
        container.storyboardInitCompleted(ContactListViewController.self) { r, c in
            c.viewModel = r ~> Contact.ListViewModel.self
        }
        container.storyboardInitCompleted(ContactAddViewController.self) { r, c in
            c.viewModel = r ~> Contact.AddViewModel.self
        }
        container.storyboardInitCompleted(ContactDetailViewController.self) { r, c in
            c.viewModel = r ~> Contact.DetailViewModel.self
        }
    }
}
