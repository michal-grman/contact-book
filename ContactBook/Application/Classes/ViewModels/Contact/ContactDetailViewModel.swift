//
//  ContactDetailViewModel.swift
//  ContactBook
//
//  Created by Michal Grman on 21/06/2017.
//  Copyright © 2017 Grman Trial. All rights reserved.
//

import Result
import ReactiveSwift
import ReactiveCocoa
import AlamofireImage

public protocol OrderBindingProtocol {
    
    func bind(using order:Contact.Order)
}

extension Contact {
    
    public class DetailViewModel: ContactBaseViewModel, ContactResultViewProtocol {
        
        public let contact = MutableProperty<Contact?>(nil)
        public let orders = MutableProperty<[Contact.Order]>([])
        
        public var pending:Signal<Bool,NoError> {
            return self.getOrdersAction.isExecuting.signal
        }
        
        public var errors:Signal<ServiceError,NoError> {
            return self.getOrdersAction.errors
        }
        
        public var success:Signal<Bool,NoError> {
            return self.getOrdersAction.values.map{ !$0.isEmpty }
        }
        
        public var changes:Signal<[Contact.Order],NoError> {
            return orders.signal
        }
        
        public lazy var getOrdersAction:Action<Void,[Contact.Order],ServiceError> = { [unowned self] in
            return Action<Void,[Contact.Order],ServiceError>{ [unowned self] in
                return self.service.reactive.getOrdersAction
                    .apply(self.contact.value)
                    .take(duringLifetimeOf: self)
                    .mapError { error -> ServiceError in
                        
                        guard case .producerFailed(let local) = error else { return .disabled }
                        return local
                        
                }
            }
        }()
        
        public override init(service: Contact.Service) {
            super.init(service: service)
            
            self.orders <~ self.getOrdersAction.values.observe(on: UIScheduler()).take(duringLifetimeOf:self)
        }
        
        func setupBindings(_ bindings: DetailViewBindings) {
            bindings.title <~ self.contact.producer.map{ $0?.name }.take(duringLifetimeOf: bindings.controller).skipNil()
            bindings.phone <~ self.contact.producer.map{ $0?.phone }.take(duringLifetimeOf: bindings.controller)
            
            let placeholder = UIImage(named: "placeholder.png")
            if let path = self.contact.value?.pictureUrl, let url = URL(string: path) {
                
                bindings.picture.af_setImage(withURL: url, placeholderImage:placeholder, imageTransition:.crossDissolve(0.3))
            } else {
                bindings.picture.image = placeholder
            }
            
        }
    }
    
}
