//
//  ContactAddViewModel.swift
//  ContactBook
//
//  Created by Michal Grman on 21/06/2017.
//  Copyright © 2017 Grman Trial. All rights reserved.
//

import Result
import ReactiveSwift
import ReactiveCocoa


extension Contact {
    
    public class AddViewModel: ContactBaseViewModel, ContactResultViewProtocol {
     
        private let form = MutableProperty<Form>(Form.Invalid)
        
        public let name = MutableProperty<String>("")
        public let phone = MutableProperty<String>("")
        public let enabled = MutableProperty<Bool>(false)
        
        public var pending:Signal<Bool,NoError> {
            return self.addContactAction.isExecuting.signal
        }
        
        public var errors:Signal<ServiceError,NoError> {
            return self.addContactAction.errors
        }
        
        public var success:Signal<Bool,NoError> {
            return self.addContactAction.values
        }
        
        public lazy var addContactAction:Action<Void,Bool,ServiceError> = { [unowned self] in
            return Action<Void,Bool,ServiceError>(enabledIf:self.enabled) { [unowned self] in
                return self.service.reactive.addContactAction
                    .apply(self.form.value)
                    .take(duringLifetimeOf: self)
                    .mapError { error -> ServiceError in
                    
                        guard case .producerFailed(let local) = error else { return .disabled }
                        return local
                        
                    }
            }
        }()
        
        override public init(service: Contact.Service) {
            super.init(service: service)
            
            self.form <~ name.combineLatest(with: phone).map{ Form(name: $0.0, phone: $0.1) }
            self.enabled <~ form.map{ $0.valid }
        }
        
        func setupBindings(_ bindings: AddViewBindings) {
            
            self.name <~ bindings.name.skipNil().take(duringLifetimeOf: self)
            self.phone <~ bindings.phone.skipNil().take(duringLifetimeOf: self)
            
            bindings.button.reactive.pressed = CocoaAction(self.addContactAction)
        }
    }
    
}
