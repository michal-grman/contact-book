//
//  ContactListViewModel.swift
//  ContactBook
//
//  Created by Michal Grman on 21/06/2017.
//  Copyright © 2017 Grman Trial. All rights reserved.
//

import Result
import ReactiveSwift
import ReactiveCocoa
import Sync
import DATASource
import CoreData

public protocol ContactBindingProtocol {
    
    func bind(using contact:Contact)
}

extension Contact {

    public class ListViewModel: ContactBaseViewModel, ContactResultViewProtocol {
        
        weak public var stack:DataStack?
        weak public var detailViewModel:DetailViewModel?
        
        public var pending:Signal<Bool,NoError> {
            return self.syncContactsAction.isExecuting.signal
        }
        
        public var errors:Signal<ServiceError,NoError> {
            return self.syncContactsAction.errors
        }
        
        public var success:Signal<Bool,NoError> {
            return self.syncContactsAction.values
        }
        
        public var add:Signal<Bool,NoError> {
            return self.addAction.values
        }
        
        public lazy var addAction:Action<Void,Bool,NoError> = {
            return Action<Void,Bool,NoError> {
                return SignalProducer<Bool,NoError>(value:true)
            }
        }()
        
        public lazy var syncContactsAction:Action<Void,Bool,ServiceError> = { [unowned self] in
            return self.service.reactive.syncContactsAction
        }()
        
        func setupSourceBindings(using tableView:UITableView, identifier:String) throws -> DATASource {
            
            guard let stack = self.stack else { throw ServiceError.noData }
            
            let request = NSFetchRequest<NSFetchRequestResult>(entityName: "\(Contact.self)")
            request.sortDescriptors = [
                NSSortDescriptor(key: "name", ascending: true),
            ]
            
            let source = DATASource(tableView: tableView, cellIdentifier: identifier, fetchRequest: request, mainContext: stack.mainContext) { cell, item, indexPath in
                
                if let cell = cell as? ContactBindingProtocol, let contact = item as? Contact {
                    cell.bind(using: contact)
                }
                
            }
            
            return source
        }
        
        func setupBindings(_ bindings: ListViewBindings) {
            bindings.item.reactive.pressed = CocoaAction(self.addAction)
        }
        
        func bindContact(using source:DATASource, at indexPath:IndexPath) -> Bool {
            
            guard let contact = source.object(indexPath) as? Contact, let vm = self.detailViewModel else { return false }
            vm.contact.value = contact
            
            return true
        }
    }
    
}
