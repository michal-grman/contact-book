//
//  ContactBaseViewModel.swift
//  ContactBook
//
//  Created by Michal Grman on 21/06/2017.
//  Copyright © 2017 Grman Trial. All rights reserved.
//

import Result
import ReactiveSwift

public protocol ContactResultViewProtocol: class {
    
    var pending:Signal<Bool,NoError> { get }
    var success:Signal<Bool,NoError> { get }
    var errors:Signal<ServiceError,NoError> { get }
}

public class ContactBaseViewModel  {
    
    deinit {
         print("Deallocated VM \(type(of: self))")
    }
    
    unowned public let service:Contact.Service
    
    public init(service:Contact.Service) {
        self.service = service
    }
}
